#include "LDDGraph.h"
#include <cstring>
#include <map>
#include "SylvanWrapper.h"

LDDGraph::~LDDGraph()=default;


void LDDGraph::setInitialState(LDDState *c) {
   m_initialstate = c;
   SylvanWrapper::setLDDState(c);
}


/*----------------------find()----------------*/
LDDState *LDDGraph::find(LDDState *c) {
    std::lock_guard lock(m_mutex);
    for (const auto & i : m_GONodes)
        if (c->mLDD == i->mLDD)
            return i;
    return nullptr;
}



LDDState *LDDGraph::insertFindByMDD(const MDD& md, bool &found) {
    std::lock_guard lock(m_mutex);
    auto res{SylvanWrapper::getLDDState(md)};
    if (res) {
        found=true;
        return res;
    }
    /*    std::cout<<"found by hash\n"; else std::cout<<"*****************************Not found bu hash\n";
        std::cout<<"Adress : "<<SylvanWrapper::getLDDState(md)<<endl;
        for (auto& i : m_GONodes) {
            if (md == i->mLDD) {
                found = true;
                //std::cout<<"found...\n";
                return i;
            }
        }
    //std::cout<<"Not found\n";*/

    res=new LDDState;
    res->mLDD = md;
    found = false;
    m_GONodes.emplace_back(res);
    SylvanWrapper::setLDDState(res);
    //std::cout<<"Adress of LDD :"<<n<<" "<<"Obtained adress"<<SylvanWrapper::getLDDState(md)<<std::endl;
    return res;
}



LDDState *LDDGraph::findSHA(unsigned char *c) {
    {
        std::lock_guard lock(m_mutex);
        for (const auto &i: m_GONodes)
            if (i->isVirtual())
                if (memcmp((char *) c, (char *) i->m_SHA2, 16) == 0)
                    return i;
    }
    return nullptr;
}

/***   Try to find an aggregate by its md5 code, else it is inserted***/
LDDState *LDDGraph::insertFindSha(unsigned char *c, LDDState *agg) {
    LDDState *res = nullptr;
    {
        std::lock_guard lock(m_mutex);
        for (const auto & i : m_GONodes) {
            if (i->isVirtual() )
                if (memcmp((char *) c, (char *) i->m_SHA2, 16) == 0) {
                    res = i;
                    break;
                }
        }
        if (res == nullptr) {
            agg->setVirtual();
            memcpy(agg->m_SHA2, c, 16);
            this->m_GONodes.push_back(agg);
        }
    }
    return res;
}

/*--------------------------------------------*/
size_t LDDGraph::findSHAPos(unsigned char *c, bool &res) {
    size_t i;
    res = false;
    {
        std::lock_guard lock(m_mutex);
        for (i = 0; i < m_GONodes.size(); ++i)
            if (memcmp(c, m_GONodes[i]->m_SHA2, 16) == 0) {
                res = true;
                return i;
            }
    }
    return i;
}

/*----------------------insert() ------------*/
void LDDGraph::insert(LDDState *c) {
    std::lock_guard lock(m_mutex);
    this->m_GONodes.push_back(c);
}


/*----------------------insert() ------------*/
void LDDGraph::insertSHA(LDDState *c) {
    c->setVirtual();
    std::lock_guard lock(m_mutex);
    this->m_GONodes.push_back(c);

}


/*----------------------Visualisation du graphe------------------------*/
void LDDGraph::printCompleteInformation() {
    size_t count_ldd = 0L;
    for (const auto & i : m_GONodes) {
        count_ldd += SylvanWrapper::lddmc_nodecount(i->mLDD);
        m_nbMarking += SylvanWrapper::getMarksCount(i->mLDD);
    }
    cout << "\n\nGRAPH SIZE : \n";
    cout << "\n\tNB LDD NODES : " << count_ldd;
    cout << "\n\tNB MARKING : " << m_nbMarking;
    cout << "\n\tNB NODES : " << m_GONodes.size();
    //cout << "\n\tNB ARCS : " << m_nbArcs << endl;

}

/*----------------------InitVisit()------------------------*/
void LDDGraph::InitVisit(LDDState *S, size_t nb) {
    if (nb <= m_nbStates) {

        for (const auto & i : S->Successors) {

            if (i.first->isVisited()) {
                ++nb;
                InitVisit(i.first, nb);
            }
        }

    }
}

/*********                  printGraph    *****/

void LDDGraph::printGraph(LDDState *s, size_t &nb) {
    if (nb <= m_nbStates) {
        cout << "\nSTATE NUMBER " << nb << " sha : " << s->getSHAValue() << " LDD v :" << s->getLDDValue() << endl;

        s->setVisited();

        for (const auto & i:  s->Successors) {
            if (!i.first->isVisited()) {
                ++nb;
                printGraph(i.first, nb);
            }
        }

    }

}


/*** Giving a position in m_GONodes Returns an LDDState ****/
LDDState *LDDGraph::getLDDStateById(const unsigned int& id) {
    return m_GONodes[id];
}

string_view LDDGraph::getTransition(uint16_t pos) {
    return m_constructor->m_net->mTransitions[pos].name;
}

string_view LDDGraph::getPlace(uint16_t pos) {
    return m_constructor->m_net->mPlaces[pos].name;
}


//void LDDGraph::setTransition(vector<string> list)

