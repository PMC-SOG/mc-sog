#ifndef TRANSSYLVAN_H
#define TRANSSYLVAN_H

#include "SylvanWrapper.h"

struct TransSylvan {
 public:
    TransSylvan()=default;
  TransSylvan(const MDD &_minus, const MDD &_plus);
  virtual ~TransSylvan();
  [[nodiscard]] MDD getMinus() const;
  [[nodiscard]] MDD getPlus() const;

 private:
    MDD m_minus, m_plus;
};

#endif  // TRANSSYLVAN_H
