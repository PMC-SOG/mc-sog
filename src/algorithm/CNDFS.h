//
// Created by ghofrane on 5/4/22.
//
#ifndef PMC_SOG_CNDFS_H
#define PMC_SOG_CNDFS_H

#include "../ModelCheckBaseMT.h"
#include <spot/tl/apcollect.hh>
#include <spot/tl/contain.hh>
#include <spot/twa/twagraph.hh>
#include <cstdint>
#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "misc/SafeDequeue.h"
#include <random>
#include "ParallelMCBase.h"

using namespace std;
typedef pair<struct myState_t *, int> coupleSuccessor;
static constexpr uint8_t MAX_THREADS = 128;




struct myState_t : public myStateBase_t {
public:
    bool* cyan ;
    bool blue{false};
    bool red{false};
    myState_t(uint16_t n) {
        cyan=new bool[n];
        memset(cyan,false,n);
    }
    ~myState_t() {
        delete [] cyan;
    }
};

class CNDFS : public ParallelMCBase {
private:

    myState_t *mInitStatePtr;
    vector<myState_t *> mlBuiltStates;
    void computeSuccessors(myState_t *state);
    static void threadHandler(void *context);
    void threadRun();
    myState_t *buildState(LDDState *left, spot::twa_graph_state *right);
    myState_t* isStateBuilt(LDDState *sogState,spot::twa_graph_state *spotState);
    void dfsBlue(myState_t *state, vector<myState_t *> &Rp, uint8_t idThread);
    void dfsRed(myState_t *state, vector<myState_t *> &Rp, uint8_t idThread);
protected:
    void getInitialState() override;
public:
    CNDFS(ModelCheckBaseMT *mcl, const spot::twa_graph_ptr &af, const uint16_t &nbTh);
    virtual ~CNDFS();


};

#endif //PMC_SOG_CNDFS_H
