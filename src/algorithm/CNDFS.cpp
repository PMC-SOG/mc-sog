//
// Created by ghofrane on 5/4/22.
//

#include "CNDFS.h"
#include "ModelCheckBaseMT.h"
#include <iostream>
#include <spot/twa/twagraph.hh>
#include <thread>
#include <vector>
#include <spot/twa/twa.hh>
#include <deque>
#include <atomic>
#include <algorithm>
#include <execution>
#include "misc/stacksafe.h"
#include <spot/twa/formula2bdd.hh>
#include <spot/tl/formula.hh>
#include <spot/twaalgos/contains.hh>
using namespace std;

CNDFS::CNDFS(ModelCheckBaseMT *mcl, const spot::twa_graph_ptr &af, const uint16_t &nbTh): ParallelMCBase(mcl,af,nbTh) {
    mThreadHandler=CNDFS::threadHandler;
    preConfigure();
}

CNDFS::~CNDFS() {

    mCycleDetected ? cout << "Property is violated...\n" : cout << "Property is verified...\n";
    // Liberate dynamic allocated memory for synchropnized product
    for (const auto &elt: mlBuiltStates)
        delete elt;
}

// Create threads


void CNDFS::threadHandler(void *context) {
    ((CNDFS *) context)->threadRun();
}

void CNDFS::threadRun() {
    volatile uint16_t idThread = mIdThread++;
    vector<myState_t *> Rp;
    dfsBlue(mInitStatePtr, Rp, idThread);
}

/*
 * Build initial state of the product automata
 */
void CNDFS::getInitialState() {
    mInitStatePtr = new myState_t(mNbTh);
    mInitStatePtr->left = mMcl->getInitialMetaState();
    mInitStatePtr->right = mAa->get_init_state();
    mInitStatePtr->isAcceptance = mAa->state_is_accepting(mAa->get_init_state());
    mlBuiltStates.emplace_back(mInitStatePtr);
}


//this function is to build a state with list of successor initially null
myState_t *CNDFS::buildState(LDDState *left, spot::twa_graph_state *right) {
    //cout<<"left : "<<left<<" right : "<<right<<endl;
    auto *buildStatePtr {new myState_t(mNbTh)};
    buildStatePtr->left = left;
    buildStatePtr->right = right;
    buildStatePtr->isAcceptance = mAa->state_is_accepting(right);
    mlBuiltStates.emplace_back(buildStatePtr);
    return buildStatePtr;
}

std::ostream &operator<<(std::ostream &Str, myState_t *state) {
    Str << "({ Sog state= " << state->left << ", BA state= " << state->right << ", acceptance= " << state->isAcceptance
        << ", red= " << state->red << ", blue= " << state->blue << " }"
        << endl;
    int i = 0;
    for (const auto &ii: state->new_successors) {
        Str << "succ num " << i << ii.first << " with transition " << ii.second << endl;
        i++;
    }
    return Str;
}

/*
 * Check whether a product state exists or not
*/
myState_t *CNDFS::isStateBuilt(LDDState *sogState, spot::twa_graph_state *spotState) {
    auto compare = [sogState, spotState](myState_t *state) {
        return (state->left == sogState && state->right == spotState);
    };
    auto result = find_if(std::execution::par,begin(mlBuiltStates), end(mlBuiltStates), compare);
    return result == end(mlBuiltStates) ? nullptr : *result;
}


//compute new successors of a product state
void CNDFS::computeSuccessors(myState_t *state) {
    if (state->succState == SuccState::done) return;
    {
        std::unique_lock lk(mMutexStatus);
        if (state->succState == SuccState::doing) {
            mDataCondWait.wait(lk, [state] { return state->succState == SuccState::done; });
            return;
        }
        state->succState = SuccState::doing;
    }

    auto sog_current_state{state->left};
    auto ba_current_state{state->right};
    while (!sog_current_state->isCompletedSucc());
    //fetch the state's atomic proposition
    vector<spot::formula> ap_source_state;
    for (const auto &vv: sog_current_state->getMarkedPlaces(mMcl->getPlaceProposition())) {
        auto name = string(mMcl->getPlace(vv));
        auto ap_state = spot::formula::ap(name);
        ap_source_state.emplace_back(ap_state);
    }
    //iterate over the successors of a current aggregate
    for (const auto &vv: sog_current_state->getUnmarkedPlaces(mMcl->getPlaceProposition())) {
        auto name = string(mMcl->getPlace(vv));
        auto ap_state = spot::formula::Not(spot::formula::ap(name));
        //if (mDictBa->var_map.find(ap_state) != mDictBa->var_map.end()) {
        ap_source_state.emplace_back(ap_state);
        //}
    }
    vector<spot::formula> ap_sog;
    //iterate over the successors of a BA state
    auto ii = mAa->succ_iter(ba_current_state);
    if (ii->first())
        do {
            for (const auto &elt: sog_current_state->Successors) {
                auto& transition = elt.second; // je récupère le numéro du transition
                if (ii->cond() == bdd_true()) {
                    std::unique_lock lk2(mMutex);
                    auto result = isStateBuilt(elt.first, (spot::twa_graph_state *) ii->dst());
                    if (result) {
                        state->new_successors.emplace_back(result, transition);
                    } else {
                        myState_t *nd = buildState(elt.first,
                                                   (spot::twa_graph_state *) (ii->dst()));
                        state->new_successors.emplace_back(nd, transition);
                    }
                } else {
                    auto name = string(mMcl->getTransition(transition)); // récuprer le nom de la transition
                    ap_sog = ap_source_state;
                    auto ap_edge = spot::formula::ap(name);// get atomic proposition corresponding to name
                    ap_sog.emplace_back(ap_edge);
                    auto pa_sog_result = spot::formula::And(ap_sog);
                    auto pa_ba_result = spot::bdd_to_formula(ii->cond(), mDictBa); // from bdd to formula
                    //if (spot::are_equivalent(pa_sog_result, pa_ba_result) ) {
                  if (mChecker.contained(pa_sog_result, pa_ba_result) || mChecker.contained(pa_ba_result, pa_sog_result)) {
                        std::unique_lock lk2(mMutex);
                        auto result = isStateBuilt(elt.first, (spot::twa_graph_state *) ii->dst());
                        if (result) {
                            state->new_successors.emplace_back(result, transition);
                        } else {
                            myState_t *nd = buildState(elt.first,
                                                       (spot::twa_graph_state *) (ii->dst()));
                            state->new_successors.emplace_back(nd, transition);
                        }
                    }
                }
            }
        } while (ii->next());
    mAa->release_iter(ii);
    state->succState = SuccState::done;
    mDataCondWait.notify_all();
}



void CNDFS::dfsRed(myState_t *state, vector<myState_t *> &Rp, uint8_t idThread) {
    if (mCycleDetected) return;
    Rp.emplace_back(state);
    //for (const auto &succ: state->new_successors) {
    for (const auto & elt :state->new_successors) {
        if (static_cast<myState_t*>(elt.first)->cyan[idThread] || static_cast<myState_t*>(elt.first)->left->isDeadLock() || static_cast<myState_t*>(elt.first)->left->isDiv()) {
            mCycleDetected = true;
            return;
        }
        if (mCycleDetected) return;
        // unvisited and not red state
        if ((!static_cast<myState_t*>(elt.first)->red && find(std::execution::par,Rp.begin(),Rp.end(),static_cast<myState_t*>(elt.first)) == Rp.end())) {
            dfsRed(static_cast<myState_t*>(elt.first), Rp, idThread);
        }
    }
}


//Perform the dfsBlue
void CNDFS::dfsBlue(myState_t *state, vector<myState_t *> &Rp, uint8_t idThread) {

    state->cyan[idThread] = true;
    if (mCycleDetected) return;
    if (state->isAcceptance) {
        if (state->left->isDeadLock() || state->left->isDiv()) {
            mCycleDetected = true;
            return;
        }
    }
    computeSuccessors(state);
  //  std::random_shuffle(state->new_successors.begin(),state->new_successors.end());

    for (const auto & elt : state->new_successors)  {
        if (!static_cast<myState_t*>(elt.first)->blue && !static_cast<myState_t*>(elt.first)->cyan[idThread]) {
            dfsBlue(static_cast<myState_t*>(elt.first), Rp, idThread);
       }
   }
    state->blue = true;
    if (state->isAcceptance)//        cout << "Acceptance state detected " << endl;[
    {
            Rp.clear();
            dfsRed(state, Rp, idThread); //looking for an accepting cycle
            //        the thread mProcessed the current state waits for all visited accepting nodes (non seed, non red) to turn red
            //        the late red coloring forces the other acceptance states to be mProcessed in post-order by the other threads
            if (mCycleDetected) return;
            bool cond;
            do {
                cond = true;
                for (const auto & e : Rp) {
                    if (e->isAcceptance && (e != state))
                        if (!e->red) {
                        cond = false;
                        break;
                    }
                }
            } while (!cond && !mCycleDetected);
            for (const auto &qu: Rp) // prune other dfsRed
            {
                qu->red = true;
            }
        if (mCycleDetected) return;

    }
    state->cyan[idThread] = false;
}



