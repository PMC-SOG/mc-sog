//
// Created by chiheb on 17/12/22.
//

#include "ParallelMCBase.h"


ParallelMCBase::ParallelMCBase(ModelCheckBaseMT *model,const spot::twa_graph_ptr &pGraph,const uint16_t &nbTh):mMcl(model), mAa(pGraph),
                                                                                                               mNbTh(nbTh) {
    mDictBa = mAa->get_dict();

}

void ParallelMCBase::preConfigure() {
    getInitialState();
    spawnThreads();
   for (auto i = 0; i < mNbTh; ++i) {
       mplThread[i]->join();
       delete mplThread[i];
   }
    delete [] mplThread;
}

void ParallelMCBase::spawnThreads() {
    mplThread=new std::thread*[mNbTh];
    for (uint16_t i = 0; i < mNbTh; ++i) {
        mplThread[i] = new std::thread(mThreadHandler,this);
        if (mplThread[i] == nullptr) {
            cout << "error: pthread creation failed. " << endl;
        }
    }
}



void (*ParallelMCBase::mThreadHandler)(void *);