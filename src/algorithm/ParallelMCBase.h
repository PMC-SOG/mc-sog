//
// Created by chiheb on 17/12/22.
//

#ifndef PMC_SOG_PARALLELMCBASE_H
#define PMC_SOG_PARALLELMCBASE_H
#include <thread>
#include <spot/tl/contain.hh>
#include "../ModelCheckBaseMT.h"

enum class SuccState {notyet,doing,done};

struct myStateBase_t {
public:
    LDDState *left;
    const spot::twa_graph_state *right;
    vector<pair<struct myStateBase_t *, int>> new_successors;
    bool isAcceptance {false};
    SuccState succState {SuccState::notyet};
};



class ParallelMCBase {
private:
    void spawnThreads();
    std::thread **mplThread;
protected:
    void preConfigure();
    virtual void getInitialState()=0;

    static void (*mThreadHandler)(void *);
    spot::bdd_dict_ptr mDictBa;
    ModelCheckBaseMT *mMcl;
    spot::twa_graph_ptr mAa;
    uint16_t mNbTh;
   // static spot::bdd_dict_ptr *m_dict_ptr;
    atomic<uint8_t> mIdThread;
    spot::language_containment_checker mChecker;
    condition_variable mCondVar;
    mutex mMutex,mMutexStatus;
    std::condition_variable mDataCondWait;
    std::atomic<bool> mCycleDetected {false};
public:
    ParallelMCBase(ModelCheckBaseMT *,const spot::twa_graph_ptr &,const uint16_t &);
};


#endif //PMC_SOG_PARALLELMCBASE_H
