//
// Created by ghofrane on 7/21/22.
//

#ifndef PMC_SOG_UFSCC_H
#define PMC_SOG_UFSCC_H
#include "ParallelMCBase.h"
#include "misc/SafeMap.h"


struct myStateU_t : public myStateBase_t {
public:
   bool mProcessed {false};
   bool mDead {false};
};


class UFSCC : public ParallelMCBase {
public:
    UFSCC(ModelCheckBaseMT *mcl, const spot::twa_graph_ptr &af, const uint16_t &nbTh);
    virtual ~UFSCC();
private:
    mutex mMutexSpot;
    SafeMap<myStateU_t *,std::set<myStateU_t *>> mS;
    myStateU_t *mInitStatePtr;
    vector<myStateU_t *> mlBuiltStates;
    static void threadHandler(void *context);
    void threadRun();
    void ufscc(myStateU_t *state, vector<myStateU_t *> &Rp, uint8_t idThread);
    void computeSuccessors(myStateU_t *state);
    myStateU_t* isStateBuilt(LDDState *sogState,spot::twa_graph_state *spotState);
    myStateU_t *buildState(LDDState *left, spot::twa_graph_state *right);
protected:
    void getInitialState() override;
};


#endif //PMC_SOG_UFSCC_H
