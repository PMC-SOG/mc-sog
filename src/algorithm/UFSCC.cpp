//
// Created by ghofrane on 7/21/22.
//

#include "UFSCC.h"
#include <spot/twa/formula2bdd.hh>

UFSCC::UFSCC(ModelCheckBaseMT *mcl, const spot::twa_graph_ptr &af, const uint16_t &nbTh) : ParallelMCBase(mcl, af, nbTh) {
    mThreadHandler = UFSCC::threadHandler;
    preConfigure();
}


void UFSCC::threadHandler(void *context) {
    ((UFSCC *) context)->threadRun();
}

void UFSCC::threadRun() {
    uint16_t idThread = mIdThread++;
    vector<myStateU_t *> Rp;
    ufscc(mInitStatePtr, Rp, idThread);
}

void UFSCC::ufscc(myStateU_t *state, vector<myStateU_t *> &Rp, uint8_t idThread) {
    if ( mCycleDetected || (state->isAcceptance && (state->left->isDeadLock() || state->left->isDiv()))) {
        mCycleDetected=true;
        return;
    }
    assert(state!= nullptr);
    Rp.emplace_back(state);
    computeSuccessors(state);
    auto setS=mS.mMap[state];//mS[state];
    auto iterVP=setS.begin();
    while (iterVP!=setS.end()) {
        auto &vp=*iterVP;
        if (!vp->mProcessed) {
            for (const auto &elt: vp->new_successors) {
                if ((state->isAcceptance && (state->left->isDeadLock() || state->left->isDiv())) || mCycleDetected ) {
                    mCycleDetected=true;
                    return;
                }
                if (static_cast<myStateU_t *>(elt.first)->mDead || static_cast<myStateU_t *>(elt.first)->mProcessed) continue;

                /*bool isNew {false};
                {

                    for (const auto wp: mS.mMap) {
                        if (std::find(Rp.begin(), Rp.end(), static_cast<myStateU_t *>(elt.first)) == Rp.end())
                            if (std::find(wp.second.begin(), wp.second.end(), static_cast<myStateU_t *>(elt.first)) != wp.second.end()) {
                                ufscc(static_cast<myStateU_t *>(elt.first), Rp, idThread);
                                isNew = true;
                                break;
                            }
                    }
                }*/
                bool isNew {mS.findState(static_cast<myStateU_t *>(elt.first),Rp)};
                if (isNew) {
                    ufscc(static_cast<myStateU_t *>(elt.first), Rp, idThread);
                }
                else {
                    while (Rp.size()>1 && !mS.equal(static_cast<myStateU_t *>(elt.first), state)) {
                        auto r=Rp.back();Rp.pop_back();
                        auto s=Rp.back();
                        mS.merge(r,s);
                    }
                    for (const auto & w: mS.mMap[state]) {
                        if (w->isAcceptance) {
                            mCycleDetected=true;
                            return;
                        }
                    }
                }
            }
            vp->mProcessed=true;
        }
        ++iterVP;
    } //End while
    for (const auto & elt : mS.mMap[state])
        elt->mDead=true;
    if (Rp.back()==state) Rp.pop_back();
}

/*
 * Build initial state of the product automata
 */
void UFSCC::getInitialState() {
    mInitStatePtr = new myStateU_t;
    mInitStatePtr->left = mMcl->getInitialMetaState();
    mInitStatePtr->right = mAa->get_init_state();
    mInitStatePtr->isAcceptance = mAa->state_is_accepting(mAa->get_init_state());
    mlBuiltStates.emplace_back(mInitStatePtr);

    set<myStateU_t*> l;
    l.emplace(mInitStatePtr);
    mS.emplace(mInitStatePtr,l);
}

//compute new successors of a product state
void UFSCC::computeSuccessors(myStateU_t *state) {
    if (state->succState == SuccState::done) return;
    {
        std::unique_lock lk(mMutexStatus);
        if (state->succState == SuccState::doing) {
            mDataCondWait.wait(lk, [state] { return state->succState == SuccState::done; });
            return;
        }
        state->succState = SuccState::doing;
    }

    auto sog_current_state{state->left};
    auto ba_current_state{state->right};
    while (!sog_current_state->isCompletedSucc());
    //fetch the state's atomic proposition
    vector<spot::formula> ap_source_state;
    for (const auto &vv: sog_current_state->getMarkedPlaces(mMcl->getPlaceProposition())) {
        auto name = string(CommonSOG::getPlace(vv));
        auto ap_state = spot::formula::ap(name);
        ap_source_state.emplace_back(ap_state);
    }
    //iterate over the successors of a current aggregate
    for (const auto &vv: sog_current_state->getUnmarkedPlaces(mMcl->getPlaceProposition())) {
        auto name = string(CommonSOG::getPlace(vv));
        auto ap_state = spot::formula::Not(spot::formula::ap(name));
        //if (mDictBa->var_map.find(ap_state) != mDictBa->var_map.end()) {
        ap_source_state.emplace_back(ap_state);
        //}
    }
    vector<spot::formula> ap_sog;
    //iterate over the successors of a BA state
    auto ii = mAa->succ_iter(ba_current_state);
    if (ii->first())
        do {
            for (const auto &elt: sog_current_state->Successors) {
                auto transition = elt.second; // je récupère le numéro du transition
                auto name = string(mMcl->getTransition(transition)); // récuprer le nom de la transition

                if (ii->cond() == bdd_true()) {
                    std::unique_lock lk2(mMutex);
                    auto result = isStateBuilt(elt.first, (spot::twa_graph_state *) ii->dst());
                    if (result) {
                        state->new_successors.emplace_back(result, transition);
                        /***** Add mS****/
                    } else {

                        myStateU_t *nd = buildState(elt.first,
                                                   (spot::twa_graph_state *) (ii->dst()));
                        state->new_successors.emplace_back(nd, transition);


                    }
                } else {
                    ap_sog = ap_source_state;
                    auto ap_edge = spot::formula::ap(name);// get atomic proposition corresponding to name
                    ap_sog.emplace_back(ap_edge);
                    auto pa_sog_result = spot::formula::And(ap_sog);
                    auto pa_ba_result = spot::bdd_to_formula(ii->cond(), mDictBa); // from bdd to formula
                    //if (spot::are_equivalent(pa_sog_result, pa_ba_result) )
                    {
                        std::unique_lock lk2(mMutex);
                        if (mChecker.contained(pa_sog_result, pa_ba_result) || mChecker.contained(pa_ba_result, pa_sog_result)) {

                            auto result = isStateBuilt(elt.first, (spot::twa_graph_state *) ii->dst());
                            if (result) {
                                state->new_successors.emplace_back(result, transition);
                            } else {
                                myStateU_t *nd = buildState(elt.first,
                                                            (spot::twa_graph_state *) (ii->dst()));
                                state->new_successors.emplace_back(nd, transition);

                            }
                        }
                    }
                }
            }
        } while (ii->next());
    mAa->release_iter(ii);
    state->succState = SuccState::done;
    mDataCondWait.notify_all();
}

/*
 * Check whether a product state exists or not
*/
myStateU_t *UFSCC::isStateBuilt(LDDState *sogState, spot::twa_graph_state *spotState) {
    auto compare = [sogState, spotState](myStateU_t *state) {
        return (state->left == sogState && state->right == spotState);
    };
    auto result = find_if(begin(mlBuiltStates), end(mlBuiltStates), compare);
    return result == end(mlBuiltStates) ? nullptr : *result;
}

//this function is to build a state with list of successor initially null
myStateU_t *UFSCC::buildState(LDDState *left, spot::twa_graph_state *right) {
    //cout<<"left : "<<left<<" right : "<<right<<endl;
    auto *buildStatePtr {new myStateU_t};
    buildStatePtr->left = left;
    buildStatePtr->right = right;
    buildStatePtr->isAcceptance = mAa->state_is_accepting(right);
    mlBuiltStates.emplace_back(buildStatePtr);
    set<myStateU_t*> l;
    l.emplace(buildStatePtr);
    mS.emplace(buildStatePtr,l);
    return buildStatePtr;
}

UFSCC::~UFSCC() {
   mCycleDetected ? cout << "Property is violated...\n" : cout << "Property is verified...\n";
   // Liberate dynamic allocated memory for synchropnized product
   for (const auto &elt: mlBuiltStates)
    delete elt;
}