#include "ModelCheckBaseMT.h"
#include "SylvanWrapper.h"

ModelCheckBaseMT::ModelCheckBaseMT(NewNet &R,int nbThread)
{
    m_nb_thread=nbThread;
    m_net=&R;
    m_nbPlaces=m_net->mPlaces.size();
}
void ModelCheckBaseMT::loadNet()
{
	m_graph=new LDDGraph(this);
	preConfigure();
}


void ModelCheckBaseMT::buildSucc ( LDDState *agregate )
{
    if ( !agregate->isVisited() ) {
        agregate->setVisited();        
        std::unique_lock<std::mutex> lk ( m_mutexBuild );
        m_condBuild.wait(lk,[&agregate]{return agregate->isCompletedSucc();});
        lk.unlock();        
    }
}

LDDState* ModelCheckBaseMT::getInitialMetaState()
{
    while ( !m_finish_initial.load(std::memory_order_release));
    LDDState *initial_metastate = m_graph->getInitialState();
    buildSucc(initial_metastate);    
    return initial_metastate;
}




