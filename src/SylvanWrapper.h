//
// Created by chiheb on 10/06/2020.
//

#ifndef PMC_SOG_SYLVANWRAPPER_H
#define PMC_SOG_SYLVANWRAPPER_H
#include <cstdint>
#include <cstddef>
#include <string>
#include <atomic>
#include "SylvanCacheWrapper.h"


/**
 * Lockless hash table (set) to store 16-byte keys.
 * Each unique key is associated with a 42-bit number.
 *
 * The set has support for stop-the-world garbage collection.
 * Methods llmsset_clear, llmsset_mark and llmsset_rehash implement garbage collection.
 * During their execution, llmsset_lookup is not allowed.
 *
 * WARNING: Originally, this table is designed to allow multiple tables.
 * However, this is not compatible with thread local storage for now.
 * Do not use multiple tables.
 */

/**
 * hash(a, b, seed)
 * equals(lhs_a, lhs_b, rhs_a, rhs_b)
 * create(a, b) -- with a,b pointers, allows changing pointers on create of node,
 *                 but must keep hash/equals same!
 * destroy(a, b)
 */


typedef uint64_t (*llmsset_hash_cb)(uint64_t, uint64_t, uint64_t);

typedef int (*llmsset_equals_cb)(uint64_t, uint64_t, uint64_t, uint64_t);

typedef void (*llmsset_create_cb)(uint64_t *, uint64_t *);

typedef void (*llmsset_destroy_cb)(uint64_t, uint64_t);

class LDDState;
typedef uint64_t MDD;       // Note: low 40 bits only
extern MDD lddmc_false; //= 0;
extern MDD lddmc_true;// = 1;

typedef struct llmsset2 {
    uint64_t *table;       // table with hashes
    uint64_t *state;
    uint8_t *data;        // table with values
    uint64_t *bitmap1;     // ownership bitmap (per 512 buckets)
    uint64_t *bitmap2;     // bitmap for "contains data"
    uint64_t *bitmapc;     // bitmap for "use custom functions"
    size_t max_size;     // maximum size of the hash table (for resizing)
    size_t table_size;   // size of the hash table (number of slots) --> power of 2!
#if LLMSSET_MASK
    size_t            mask;         // size-1
#endif
    size_t f_size;
    llmsset_hash_cb hash_cb;      // custom hash function
    llmsset_equals_cb equals_cb;    // custom equals function
    llmsset_create_cb create_cb;    // custom create function
    llmsset_destroy_cb destroy_cb;  // custom destroy function
    std::atomic<int16_t> threshold;    // number of iterations for insertion until returning error
} *llmsset2_t;

/**
 * LDD node structure
 *
 * RmRR RRRR RRRR VVVV | VVVV DcDD DDDD DDDD (little endian - in memory)
 * VVVV RRRR RRRR RRRm | DDDD DDDD DDDc VVVV (big endian)
 */
typedef struct __attribute__((packed)) mddnode {
    uint64_t a, b;
} *mddnode_t; // 16 bytes

/**
 * Implementation of external references
 * Based on a hash table for 40-bit non-null values, linear probing
 * Use tombstones for deleting, higher bits for reference count
 */
typedef struct {
    uint64_t *refs_table;           // table itself
    size_t refs_size;               // number of buckets

    /* helpers during resize operation */
    std::atomic<uint32_t> refs_control; // control field
    uint64_t *refs_resize_table;    // previous table
    size_t refs_resize_size;        // size of previous table
    size_t refs_resize_part;        // which part is next
    size_t refs_resize_done;        // how many parts are done
} refs_table2_t;
/************************************** From lace.h******/



typedef struct lddmc_refs_internal {
    const MDD **pbegin, **pend, **pcur;
    MDD *rbegin, *rend, *rcur;
} *lddmc_refs_internal_t;


#ifndef cas
#define cas(ptr, old, new) (__sync_bool_compare_and_swap((ptr),(old),(new)))
#endif
/* Typical cacheline size of system architectures */
#ifndef LINE_SIZE
#define LINE_SIZE 64
#endif
// The LINE_SIZE is defined in lace.h
  const uint64_t CL_MASK = ~(((LINE_SIZE) / 8) - 1);
  const uint64_t CL_MASK_R = ((LINE_SIZE) / 8) - 1;
/* 40 bits for the index, 24 bits for the hash */
#define MASK_INDEX ((uint64_t)0x000000ffffffffff)
#define MASK_HASH  ((uint64_t)0xffffff0000000000)

namespace SylvanWrapper {
    static llmsset2_t m_nodes;
    static size_t m_table_min, m_table_max, m_cache_min, m_cache_max;
    static std::atomic<int> m_gc;

    static refs_table2_t m_lddmc_refs; // External references
    static refs_table2_t m_lddmc_protected;// External references
    static int m_lddmc_protected_created;// External references

    static lddmc_refs_internal_t m_sequentiel_refs;
    //  lddmc_refs_internal_t m_lddmc_refs_key; // This is local to a thread

    static uint32_t m_g_created;
    uint64_t getMarksCount(MDD cmark);

    llmsset2_t llmsset_create(size_t initial_size, size_t max_size);

    void llmsset_set_size(llmsset2_t dbs, size_t size);

    void sylvan_set_limits(size_t memorycap, int table_ratio, int initial_ratio);

    void sylvan_init_package();
    void *llmsset_index_to_ptr(size_t index);
     mddnode_t GETNODE(MDD mdd);// { return ((mddnode_t) llmsset_index_to_ptr( mdd)); }



    // Explore basic operations : to isolated to another file
    inline uint64_t __attribute__((unused)) mddnode_getright(mddnode_t n) {
        return (n->a & 0x0000ffffffffffff) >> 1;
    }

    inline uint32_t __attribute__((unused)) mddnode_getvalue(mddnode_t n) {
        return *(uint32_t *) ((uint8_t *) n + 6);
    }

    inline uint64_t __attribute__((unused)) mddnode_getdown(mddnode_t n) {
        return n->b >> 17;
    }

    void sylvan_init_ldd();

    void refs_create(refs_table2_t *tbl, size_t _refs_size);

    void protect_create(refs_table2_t *tbl, size_t _refs_size);

    void init_gc_seq();

    size_t llmsset_get_size(const llmsset2_t dbs);

    size_t seq_llmsset_count_marked(llmsset2_t dbs);

    size_t llmsset_count_marked_seq(llmsset2_t dbs, size_t first, size_t count);

    void displayMDDTableInfo();

    MDD lddmc_cube(uint32_t *values, size_t count);

    MDD lddmc_makenode(uint32_t value, MDD ifeq, MDD ifneq);

    mddnode_t LDD_GETNODE(MDD mdd);

    MDD __attribute__((unused)) lddmc_refs_push(MDD lddmc);

    void __attribute__((unused)) lddmc_refs_pop(long amount);

    MDD __attribute__((noinline)) lddmc_refs_refs_up(lddmc_refs_internal_t lddmc_refs_key, MDD res);

    size_t lddmc_nodecount(MDD mdd);

    MDD lddmc_union_mono( MDD a, MDD b);
    MDD lddmc_intersect(MDD a, MDD b);
    bool isSingleMDD(MDD mdd);

    int get_mddnbr(MDD mdd, unsigned int level);

    MDD ldd_divide_rec(MDD a, int level);

    MDD ldd_divide_internal(MDD a, int current_level, int level);

    MDD ldd_minus(const MDD& a,const MDD& b);

    MDD lddmc_firing_mono(MDD cmark, const MDD& minus, const MDD& plus);

    size_t lddmc_nodecount_mark(const MDD& mdd);

    void lddmc_nodecount_unmark(MDD mdd);

    MDD lddmc_make_copynode(MDD ifeq, MDD ifneq);

    void lddmc_refs_init();
    void lddmc_refs_init_task();

    // sylvan_int.h
    inline void __attribute__((unused))
    mddnode_makecopy(mddnode_t n, uint64_t right, uint64_t down) {
        n->a = right << 1;
        n->b = ((down << 1) | 1) << 16;
    }

    inline uint8_t __attribute__((unused)) mddnode_getcopy(mddnode_t n) {
        return n->b & 0x10000 ? 1 : 0;
    }

    inline uint8_t __attribute__((unused)) mddnode_getmark(mddnode_t n) {
        return n->a & 1;
    }

    inline void __attribute__((unused)) mddnode_setmark(mddnode_t n, uint8_t mark) {
        n->a = (n->a & 0xfffffffffffffffe) | (mark ? 1 : 0);
    }

    //sylvan_table.c
    inline void __attribute__((unused))
    mddnode_make(mddnode_t n, uint32_t value, uint64_t right, uint64_t down) {
        n->a = right << 1;
        n->b = down << 17;
        *(uint32_t *) ((uint8_t *) n + 6) = value;
    }

    inline uint64_t
    llmsset_lookup2(uint64_t a, uint64_t b, int *created, const int custom);
    uint64_t llmsset_lookup(const uint64_t& a, const uint64_t& b, int *created);

      uint64_t llmsset_hash(const uint64_t a, const uint64_t b, const uint64_t seed);

      uint64_t claim_data_bucket();
      void release_data_bucket(uint64_t index);
      void set_custom_bucket(uint64_t index, int on);
     void llmsset_reset_region();
     MDD lddmc_project( const MDD mdd, const MDD proj);
     int isGCRequired();
     void convert_wholemdd_stringcpp(MDD cmark,std::string &res);
     void sylvan_gc_seq();
     bool isFirable(MDD cmark, MDD minus);
     int is_custom_bucket(const llmsset2_t dbs, uint64_t index);
     int llmsset_rehash_bucket(const llmsset2_t dbs, uint64_t d_idx);
     int llmsset_rehash_seq (llmsset2_t dbs);
     int llmsset_rehash_par_seq(llmsset2_t  dbs, size_t  first, size_t  count);
     void llmsset_clear_hashes_seq( llmsset2_t dbs);
     void sylvan_rehash_all();
     void llmsset_destroy(llmsset2_t dbs, size_t first, size_t count);
     void llmsset_destroy_unmarked( llmsset2_t dbs);
     void llmsset_clear_data(llmsset2_t dbs);
     void ldd_gc_mark_protected();
     void ldd_refs_mark_p_par( const MDD** begin, size_t count);
     void ldd_gc_mark_rec(MDD mdd);
     int  llmsset_mark(const llmsset2_t dbs, uint64_t index);
     void ldd_refs_mark_r_par( MDD* begin, size_t count);
    LDDState *getLDDState(MDD mdd);
    void setLDDState(LDDState *state);
}


#endif //PMC_SOG_SYLVANWRAPPER_H
