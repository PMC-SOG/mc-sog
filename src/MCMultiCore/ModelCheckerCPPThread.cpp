#include "ModelCheckerCPPThread.h"
#include <functional>
#include <iostream>
#include <unistd.h>

#include "SylvanWrapper.h"

#define GETNODE(mdd) ((mddnode_t)llmsset_index_to_ptr(nodes, mdd))

using namespace std;

ModelCheckerCPPThread::ModelCheckerCPPThread(NewNet &R, const int& nbThread) :
        ModelCheckBaseMT(R, nbThread) {
    cout<<__func__ <<endl;
}


void ModelCheckerCPPThread::preConfigure() {
    initializeLDD();
    loadNetFromFile();
    ComputeTh_Succ();
}

/*
 * Compute SOG without reduction
 */
void ModelCheckerCPPThread::Compute_successors() {

    list<int> fire;
    SylvanWrapper::lddmc_refs_init();
    auto lambda=[&fire,this]
    {
        auto *c  {new LDDState};
        MDD Complete_meta_state {Accessible_epsilon(m_initialMarking)};
        SylvanWrapper::lddmc_refs_push(Complete_meta_state);
        fire = firable_obs(Complete_meta_state);
        c->mLDD = Complete_meta_state;
        c->mDeadlock= Set_Bloc(Complete_meta_state);
        c->mDiv= Set_Div(Complete_meta_state);
        m_graph->setInitialState(c);
        m_graph->insert(c);
        m_common_stack.push(Pair(couple(c, Complete_meta_state), fire));
        m_condStack.notify_one();
        m_finish_initial.store(true,std::memory_order_release);
    };
    std::call_once(mResFlag,lambda);
    Pair e;
    do {
        {
            std::unique_lock lk(m_mutexStack);
            m_condStack.wait(lk, [this](){return  this->hasToProcess();});
        }
        if (m_common_stack.try_pop(e) && !m_finish.load(std::memory_order_acquire)) {
            while (!e.second.empty() && !m_finish.load(std::memory_order_acquire)) {
                int t {*e.second.begin()};
                e.second.remove(t);
                MDD reduced_meta {Accessible_epsilon(get_successor(e.first.second, t))};
                //SylvanWrapper::lddmc_refs_push(reduced_meta);
                bool res;
                LDDState *pos {m_graph->insertFindByMDD(reduced_meta, res)};
                if (!res) {
                    fire = firable_obs(reduced_meta);
                    pos->mDeadlock= Set_Bloc(reduced_meta);
                    pos->mDiv= Set_Div(reduced_meta);
                    m_common_stack.push(Pair(couple(pos, reduced_meta), fire));
                    m_condStack.notify_one();
                }
                auto succ {LDDEdge(pos, t)};
                e.first.first->Successors.emplace_back(succ);
            }
            e.first.first->setCompletedSucc();
            m_condBuild.notify_one();
        } //end advance
    } while (!m_finish.load(std::memory_order_acquire));

}


void ModelCheckerCPPThread::ComputeTh_Succ() {
    pthread_barrier_init(&m_barrier_builder, nullptr, m_nb_thread + 1);
    m_finish.store(false,std::memory_order_release);
    for (int i = 0; i < m_nb_thread; ++i) {
        m_list_thread[i] = new thread(&ModelCheckerCPPThread::Compute_successors, this);
        if (m_list_thread[i] == nullptr) {
            cout << "error: pthread creation failed. "  << endl;
        }
    }
}

ModelCheckerCPPThread::~ModelCheckerCPPThread() {
    m_finish.store(true,std::memory_order_release);
    m_condStack.notify_all();
    for (int i {0};i<m_nb_thread;++i) {
        m_list_thread[i]->join();
        delete m_list_thread[i];
    }
}

bool ModelCheckerCPPThread::hasToProcess() const {
    return m_finish.load(std::memory_order_acquire) || !m_common_stack.empty();
}
