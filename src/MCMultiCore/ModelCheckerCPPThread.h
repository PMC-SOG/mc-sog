#ifndef ModelCheckerCPPThread_H
#define ModelCheckerCPPThread_H
#include "ModelCheckBaseMT.h"
#include "misc/stacksafe.h"
#include "misc/SafeDequeue.h"

#include <atomic>
#include <thread>
#include <mutex>
#include <condition_variable>
typedef pair<LDDState *, int> couple_th;

/*
 * Multi-threading with C++14 Library
 */
class ModelCheckerCPPThread : public ModelCheckBaseMT
{
public:
    ModelCheckerCPPThread(NewNet &R,const int& nbThread);
    ~ModelCheckerCPPThread();
    void Compute_successors();
    void ComputeTh_Succ();
private:

    std::once_flag mResFlag;
    void preConfigure();
    bool hasToProcess() const;
    SafeDequeue<Pair> m_common_stack;
    pthread_barrier_t m_barrier_builder;
    std::condition_variable m_condStack;
    mutable std::mutex m_mutexStack;
    thread* m_list_thread[32];
};
#endif

