//
// Created by chiheb on 28/12/22.
//

#include "SafeMap.h"
#include "algorithm/UFSCC.h"

template<typename T,typename V>
typename std::map<T,V>::iterator SafeMap<T,V>::find(const T & x) {
    std::shared_lock lk(mMutex);
    return mMap.find(x);
}

template<typename T,typename V>
void SafeMap<T,V>::emplace(T& t,V& v) {
    std::lock_guard lk(mMutex);
    mMap.emplace(t,v);
}

template<typename T,typename V>
void SafeMap<T,V>::merge(T& dest,T& src) {
    std::lock_guard lk(mMutex);
    mMap[dest].merge(mMap[src]);
    mMap[src]=mMap[dest];
}

template<typename T,typename V>
bool SafeMap<T,V>::equal(const T& r, const T& s) const {
    std::shared_lock lk(mMutex);
    return mMap[s] == mMap[r];
}

template<typename T,typename V>
bool SafeMap<T,V>::findState(const T& state,const  std::vector<T>& Rp) {
    bool isNew {false};
    std::shared_lock lk(mMutex);
    for (const auto & wp: mMap) {
        if (std::find(wp.second.begin(), wp.second.end(), state) != wp.second.end())
        if (std::find(Rp.begin(), Rp.end(), wp.first) == Rp.end())
             {
                isNew = true;
                break;
            }
    }
    return isNew;
}

template class SafeMap<myStateU_t *,std::set<myStateU_t *>>;