//
// Created by chiheb on 24/03/23.
//

#ifndef PMC_SOG_SPINLOCK_H
#define PMC_SOG_SPINLOCK_H


#include <thread>
#include <atomic>
class Spinlock{
    std::atomic_flag flag;
public:
    Spinlock(): flag(ATOMIC_FLAG_INIT) {}
    void lock(){
        while(flag.test_and_set(std::memory_order_acquire));
    }
    void unlock(){
        flag.clear(std::memory_order_release);
    }
};


#endif //PMC_SOG_SPINLOCK_H
