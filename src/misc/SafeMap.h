//
// Created by chiheb on 28/12/22.
//

#ifndef PMC_SOG_SAFEMAP_H
#define PMC_SOG_SAFEMAP_H
#include <mutex>
#include <shared_mutex>
#include <map>
#include <vector>
//#include "algorithm/UFSCC.h"

template<typename T,typename V>
class SafeMap {

private:
    mutable std::shared_mutex mMutex;

public:
    std::map<T,V> mMap;
    typename std::map<T,V>::iterator find(const T & );
    void emplace(T&,V&);
    void merge(T&,T&);
    bool equal(const T&, const T&) const;
    bool findState(const T&,const  std::vector<T>& );
};


#endif //PMC_SOG_SAFEMAP_H
