//
// Created by chiheb on 01/05/23.
//

#ifndef PMC_SOG_ARRAY_H
#define PMC_SOG_ARRAY_H


#include <cstddef>

template<typename T>
struct Array {
    T* data;
    size_t size;
    Array();
    explicit Array(size_t size);
    ~Array();
    void resize(size_t s);
    T* begin() const;
    T* end() const;
};


#endif //PMC_SOG_ARRAY_H
