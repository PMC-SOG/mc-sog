//
// Created by chiheb on 01/05/23.
//

#include "Array.h"
#include "TransSylvan.h"
template<typename T>
Array<T>::Array():data(nullptr),size(0) {}

template<typename T>
Array<T>::Array(size_t s):size(s) {
    data=new T[size];
}

template<typename T>
Array<T>::~Array() {
    if (data) delete[] data;
}

template<typename T>
void Array<T>::resize(size_t s) {
    if (data) delete[] data;
    size=s;
    data = new T[size];
}

template<typename T>
T* Array<T>::begin() const {
    return data;
}
template<typename T>
T* Array<T>::end() const {
    return &data[size];
}


template class Array<struct TransSylvan>;